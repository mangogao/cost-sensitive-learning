
#from runWeka import *
from runWeka1fnl import *

from srcCost.ourmethod3 import mainOurMethod3
from srcCost.CSoldmatrix import CSoldMatrix
from srcCost.CSnewmatrix2 import CSnewMatrix
from srcCost.NoCSnew import NoCSnewMethod
from srcCost.OriginalNonCS import OriginalNonCSMethod
from srcCost.compute-probability import mainOurMethod3
import numpy as np

def computeOneBasic(myWeka, method ):
  print '####### --------- start compute cost --- in computeOneBasic () '
  print ' one file is : ', myWeka.basicOutNB
  prolistDT = []
  prolistNB = []
  prolistSVM = []
  prolistNN = []
  prolistLR = []
  prolistNB = method( myWeka.basicOutNB )
  prolistDT = method( myWeka.basicOutDT )
  (costMLP, fpMLP, fnMLP, MLPVnum) = method( myWeka.basicOutMLP )
  (costSMO, fpSMO, fnSMO, SMOVnum) = method( myWeka.basicOutSMO )
  (costL, fpL, fnL, LVnum)      = method( myWeka.basicOutLogit )

  costlist = [costNB, fpNB, fnNB, NBVnum, costDT, fpDT, fnDT, DTVnum, costMLP, fpMLP, fnMLP, MLPVnum,
              costSMO, fpSMO, fnSMO, SMOVnum, costL, fpL, fnL, LVnum]
  return costlist

def computeMetaCost(myWeka, method):
  print '####### --------- start compute cost --- in computeMetaCost () '
  print ' one file is : ', myWeka.metaOutNB
  (costNB, fpNB, fnNB, NBVnum) = method( myWeka.metaOutNB )
  (costDT, fpDT, fnDT, DTVnum) = method( myWeka.metaOutDT )
  (costMLP, fpMLP, fnMLP, MLPVnum) = method( myWeka.metaOutMLP )
  (costSMO, fpSMO, fnSMO, SMOVnum) = method( myWeka.metaOutSMO )
  (costL, fpL, fnL, LVnum)      = method( myWeka.metaOutLogit )

  costlist = [costNB, fpNB, fnNB, NBVnum, costDT, fpDT, fnDT, DTVnum, costMLP, fpMLP, fnMLP, MLPVnum,
              costSMO, fpSMO, fnSMO, SMOVnum, costL, fpL, fnL, LVnum]
  return costlist

def computeCSCost(myWeka, method):
  print '####### --------- start compute cost --- in computeCSCost () '
  print ' one file is : NB ', myWeka.csOutNB
  print ' one file is : DT ', myWeka.csOutDT
  print ' one file is : MLP ', myWeka.csOutMLP
  print ' one file is : SMO ', myWeka.csOutSMO
  print ' one file is : Logit ', myWeka.csOutLogit
  (costNB, fpNB, fnNB, NBVnum) = method( myWeka.csOutNB )
  (costDT, fpDT, fnDT, DTVnum) = method( myWeka.csOutDT )
  (costMLP, fpMLP, fnMLP, MLPVnum) = method( myWeka.csOutMLP )
  (costSMO, fpSMO, fnSMO, SMOVnum) = method( myWeka.csOutSMO )
  (costL, fpL, fnL,LVnum)      = method( myWeka.csOutLogit )

  costlist = [costNB, fpNB, fnNB, NBVnum, costDT, fpDT, fnDT, DTVnum, costMLP, fpMLP, fnMLP, MLPVnum,
              costSMO, fpSMO, fnSMO, SMOVnum, costL, fpL, fnL, LVnum]

  return costlist




def computeOneTime():
  myWeka = CWeka()
  myWeka.main()

  r1 = computeOneBasic(myWeka, mainOurMethod3)
  r2 = computeOneBasic(myWeka, CSoldMatrix)
  r3 = computeOneBasic(myWeka, CSnewMatrix)


 # costMatrix2 = '[0.0 5456; 8632  4289]'
  #costMatrixCL2 = '[0.0 3451; 2728 0.0]'
  #costMatrixMe2 = '[0.0 3451; 2728 1370]'
  costMatrixCL2 = '[0.0 6902; 5456 0.0]'
  costMatrixMe2 = '[0.0 6902; 5456 2740]'
  myWeka.runMetaClf(costMatrixMe2)
  myWeka.runCsClf(costMatrixCL2)
  r4 = computeMetaCost(myWeka, NoCSnewMethod )
  r5 = computeCSCost(myWeka, NoCSnewMethod )


 # costMatrix1 = '[0.0 11405.0; 14581.0 10243.0]'
 # costMatrix1 = '[61000 68650; 63792 0]'
 # costMatrix1 = '[95919 103914; 95919 0]'
  costMatrixCL1 = '[0.0 13632; 12186 0.0]'
  costMatrixMe1 = '[0.0 13632; 12186 9470]'
  myWeka.runMetaClf(costMatrixMe1)
  myWeka.runCsClf(costMatrixCL1)
  r6 = computeMetaCost(myWeka, OriginalNonCSMethod )
  r7 = computeCSCost(myWeka, OriginalNonCSMethod )




  myWeka.runOneSideClf()
  r8 = computeOneBasic(myWeka, NoCSnewMethod)
  r9 = computeOneBasic(myWeka, OriginalNonCSMethod)

  myWeka.runSmoteClf()
  r10 = computeOneBasic(myWeka, NoCSnewMethod)
  r11 = computeOneBasic(myWeka, OriginalNonCSMethod)


  allR = r1 + r2 + r3 + r4 + r5 + r6 + r7 + r8 + r9 + r10 + r11

  print allR

  print '###OneTimeNB  ', r1[0], r2[0], r3[0], r4[0], r5[0], r6[0], r7[0], r8[0], r9[0], r10[0], r11[0]

  #print '###OneTimeDT  ', r1[3], r2[3], r3[3], r4[3], r5[3], r6[3], r7[3], r8[3], r9[3], r10[3], r11[3]
  print '###OneTimeDT  ', r1[4], r2[4], r3[4], r4[4], r5[4], r6[4], r7[4], r8[4], r9[4], r10[4], r11[4]
  #print '###OneTimeNN  ', r1[6], r2[6], r3[6], r4[6], r5[6], r6[6], r7[6], r8[6], r9[6], r10[6], r11[6]
  print '###OneTimeNN  ', r1[8], r2[8], r3[8], r4[8], r5[8], r6[8], r7[8], r8[8], r9[8], r10[8], r11[8]
  #print '###OneTimeLR  ', r1[12], r2[12], r3[12], r4[12], r5[12], r6[12], r7[12], r8[12], r9[12], r10[12], r11[12]
  print '###OneTimeLR  ', r1[16], r2[16], r3[16], r4[16], r5[16], r6[16], r7[16], r8[16], r9[16], r10[16], r11[16]
  #print '###OneTimeSMO  ', r1[9], r2[9], r3[9], r4[9], r5[9], r6[9], r7[9], r8[9], r9[9], r10[9], r11[9]
  print '###OneTimeSMO  ', r1[12], r2[12], r3[12], r4[12], r5[12], r6[12], r7[12], r8[12], r9[12], r10[12], r11[12]
  print 'all results size : ', len(allR )

  return allR


def runMultiTime():
  num = 50
  #rSize = 165
  rSize = 220
  result = np.array([0]*rSize)
  for t in range( num ):
    print '\n\n'
    print '**'*40
    print '$$'*40
    print '------------- start  to run Id : ', t+1
    r = computeOneTime()
    nr = np.array(r)
    result += nr
    print ' accumulate results :  '
    print result

  print '\n\n'
  print '====='* 20
  print 'all avg results : '
  avgResult = result/float(num)
  print avgResult
  print '\n ------ separate results : '
  #M = 15
  M = 20
  for i in range(11):
    print ' i : ', i+1
    print avgResult[i*M: (i+1)*M]








if __name__ == '__main__':

  runMultiTime()
 # computeOneTime()





