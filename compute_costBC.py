
#from runWeka import *
from runWeka1BC import *

from srcCostBC2.ourmethod3 import mainOurMethod3
from srcCostBC2.CSoldmatrix import CSoldMatrix
from srcCostBC2.CSnewmatrix2 import CSnewMatrix
from srcCostBC2.NoCSnew import NoCSnewMethod
from srcCostBC2.OriginalNonCS import OriginalNonCSMethod
import numpy as np

def computeOneBasic(myWeka, method ):
  print '####### --------- start compute cost --- in computeOneBasic () '
  print ' one file is : ', myWeka.basicOutNB
  (costNB, fpNB, fnNB) = method( myWeka.basicOutNB )
  (costDT, fpDT, fnDT) = method( myWeka.basicOutDT )
  (costMLP, fpMLP, fnMLP) = method( myWeka.basicOutMLP )
  (costSMO, fpSMO, fnSMO) = method( myWeka.basicOutSMO )
  (costL, fpL, fnL)      = method( myWeka.basicOutLogit )

  costlist = [costNB, fpNB, fnNB, costDT, fpDT, fnDT, costMLP, fpMLP, fnMLP, costSMO, fpSMO, fnSMO, costL, fpL, fnL ]
  return costlist

def computeMetaCost(myWeka, method):
  print '####### --------- start compute cost --- in computeMetaCost () '
  print ' one file is : ', myWeka.metaOutNB
  (costNB, fpNB, fnNB) = method( myWeka.metaOutNB )
  (costDT, fpDT, fnDT) = method( myWeka.metaOutDT )
  (costMLP, fpMLP, fnMLP) = method( myWeka.metaOutMLP )
  (costSMO, fpSMO, fnSMO) = method( myWeka.metaOutSMO )
  (costL, fpL, fnL)      = method( myWeka.metaOutLogit )

  costlist = [costNB, fpNB, fnNB, costDT, fpDT, fnDT, costMLP, fpMLP, fnMLP, costSMO, fpSMO, fnSMO, costL, fpL, fnL ]
  return costlist

def computeCSCost(myWeka, method):
  print '####### --------- start compute cost --- in computeCSCost () '
  print ' one file is : NB ', myWeka.csOutNB
  print ' one file is : DT ', myWeka.csOutDT
  print ' one file is : MLP ', myWeka.csOutMLP
  print ' one file is : SMO ', myWeka.csOutSMO
  print ' one file is : Logit ', myWeka.csOutLogit
  (costNB, fpNB, fnNB) = method( myWeka.csOutNB )
  (costDT, fpDT, fnDT) = method( myWeka.csOutDT )
  (costMLP, fpMLP, fnMLP) = method( myWeka.csOutMLP )
  (costSMO, fpSMO, fnSMO) = method( myWeka.csOutSMO )
  (costL, fpL, fnL)      = method( myWeka.csOutLogit )

  costlist = [costNB, fpNB, fnNB, costDT, fpDT, fnDT, costMLP, fpMLP, fnMLP, costSMO, fpSMO, fnSMO, costL, fpL, fnL ]

  return costlist




def computeOneTime( myWeka, trfile, tsfile ):
  #myWeka = CWeka()
  myWeka.main(trfile, tsfile)

  r1 = computeOneBasic(myWeka, mainOurMethod3)
  r2 = computeOneBasic(myWeka, CSoldMatrix)
  r3 = computeOneBasic(myWeka, CSnewMatrix)


 # costMatrix2 = '[0.0 5456; 8632  4289]'
  costMatrixCL2 = '[0 19266; 11271 0]'
 # costMatrixMe2 = '[0 38545; 30550 0]'

  costMatrixMe2 = '[0 19266; 11271 0]'
 # costMatrixCL2 = '[0 25978.5; 23979.75 0]'
 # costMatrixMe2 = '[23979.75 25978.5; 23979.75 0]'
  myWeka.runMetaClf(costMatrixMe2)
  myWeka.runCsClf(costMatrixCL2)
  r4 = computeMetaCost(myWeka, NoCSnewMethod )
  r5 = computeCSCost(myWeka, NoCSnewMethod )

  costMatrixMe1 = '[0 37176; 29181 0]'
  costMatrixCL1 = '[0 37176; 29181 0]'

 # costMatrix1 = '[0.0 11405.0; 14581.0 10243.0]'
 # costMatrix1 = '[61000 68650; 63792 0]'
 # costMatrix1 = '[95919 103914; 95919 0]'
 # costMatrixCL1 = '[0 45205; 37210 0]'
 # costMatrixMe1 = '[37210 45205; 37210 0]'
 # costMatrixMe1 = '[0 45205; 37210 0]'
  myWeka.runMetaClf(costMatrixMe1)
  myWeka.runCsClf(costMatrixCL1)
  r6 = computeMetaCost(myWeka, OriginalNonCSMethod )
  r7 = computeCSCost(myWeka, OriginalNonCSMethod )




  myWeka.runOneSideClf()
  r8 = computeOneBasic(myWeka, NoCSnewMethod)
  r9 = computeOneBasic(myWeka, OriginalNonCSMethod)

  myWeka.runSmoteClf()
  r10 = computeOneBasic(myWeka, NoCSnewMethod)
  r11 = computeOneBasic(myWeka, OriginalNonCSMethod)


  allR = r1 + r2 + r3 + r4 + r5 + r6 + r7 + r8 + r9 + r10 + r11

  print allR

  print '###OneTimeNB  ', r1[0], r2[0], r3[0], r4[0], r5[0], r6[0], r7[0], r8[0], r9[0], r10[0], r11[0]

  print '###OneTimeDT  ', r1[3], r2[3], r3[3], r4[3], r5[3], r6[3], r7[3], r8[3], r9[3], r10[3], r11[3]

  print '###OneTimeNN  ', r1[6], r2[6], r3[6], r4[6], r5[6], r6[6], r7[6], r8[6], r9[6], r10[6], r11[6]
  print '###OneTimeLR  ', r1[12], r2[12], r3[12], r4[12], r5[12], r6[12], r7[12], r8[12], r9[12], r10[12], r11[12]
  print '###OneTimeSMO  ', r1[9], r2[9], r3[9], r4[9], r5[9], r6[9], r7[9], r8[9], r9[9], r10[9], r11[9]
  print 'all results size : ', len(allR )

  return allR


def runCreateDataSet():
  ''' run this when only need to create random datasets
  the 'num' should be equal or less than that used in runMultiTime() function
  '''
  num = 20
  datafile = 'data3/data3train.csv'
  myWeka = CWeka()
  (trlist, tslist) = myWeka.generateRandomData(datafile, num)




def runMultiTime():
  num = 20
  rSize = 165
  result = np.array([0]*rSize)

  datafile = 'data3/data3train.csv'
  myWeka = CWeka()
  #(trlist, tslist) = myWeka.generateRandomData(datafile, num)
  (trlist, tslist) = myWeka.generateRandomDataOnlyFileName(datafile, num)


  #return

  for t in range( num ):
    print '\n\n'
    print '**'*40
    print '$$'*40
    print '------------- start  to run Id : ', t+1
    trfile = trlist[t]
    tsfile = tslist[t]
    r = computeOneTime(myWeka, trfile, tsfile)
    nr = np.array(r)
    result += nr
    print ' accumulate results :  '
    print result

  print '\n\n'
  print '====='* 20
  print 'all avg results : '
  avgResult = result/float(num)
  print avgResult
  print '\n ------ separate results : '
  M = 15
  for i in range(11):
    print ' i : ', i+1
    print avgResult[i*M: (i+1)*M]








if __name__ == '__main__':

   runCreateDataSet()
 # runMultiTime()
 # computeOneTime()





