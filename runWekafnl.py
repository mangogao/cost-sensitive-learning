import sys
import os
import random


class CWeka:
  def __init__(self):
    #self.wekaDir="/uusoc/scratch/res/nlp/haibo/tools/weka/weka-3-6-9/weka.jar"
    #self.wekaDir="/uusoc/scratch/res/nlp/haibo/tools/weka/weka-3-6-12/weka.jar"
    self.wekaDir='/home/yuanyuag/tools/weka.jar'

  def convertCsv2Arff(self, infile, outfile):
    cmd = 'java -Xmx3g -cp '+self.wekaDir+ '  weka.core.converters.CSVLoader  ' + infile + ' > ' + outfile
    print 'cmdConvert: ', cmd
    os.system(cmd)


  def readAFile(self, datafile):
    tlist = []
    datalist = []
    In = open(datafile, 'rU')
    isData = False
    linelist = In.readlines()
    dataSize = len(linelist)
    K = 0
    for i, line in enumerate(linelist):
      tlist.append(line)
      if line.strip().lower() == '@data':
        K = i
        break
    for line in linelist[K+1:]:
      line = line.strip()
      if len(line) > 0:
        datalist.append(line)

    print 'all dataSize : ',len(datalist)
    In.close()


    return tlist, datalist


  def splitTrainTest(self, datafile):
    ''' 2/3 train, 1/3 test  '''
    trainfile = datafile.replace('.', '_') + '.train.arff'
    testfile  = datafile.replace('.', '_') + '.test.arff'
    (tlist, datalist) = self.readAFile(datafile)
    dataSize = len(datalist)
    trainSize = int( float(2)/float(3) * dataSize )
    print 'dataSize : ', dataSize
    random.shuffle(datalist)
    trainlist = datalist[:trainSize]
    testlist  = datalist[trainSize:]

    tlist = self.evaluateTitle(tlist)

    self.writeOutFile(tlist, trainlist, trainfile)
    self.writeOutFile(tlist, testlist, testfile)
    return trainfile, testfile

  def evaluateTitle(self, tlist):
    print '### evaluate attribute part ...'
    tmplist = []
    for line in tlist:
      line = line.strip()
      if line.startswith('@attribute success'):
        if line == '@attribute success {N,Y}':
          line = '@attribute success {Y,N}'
          print 'change attribute success to {Y,N}'
        elif line == '@attribute success {Y,N}':
          pass
        else:
          print 'Error, attribute success '
          print 'error line : ', line
          sys.exit()
      tmplist.append(line)
    return tmplist


  def writeOutFile(self, tlist, datalist, outfile):
    Out = open(outfile, 'w')
    for line in tlist:
      Out.write(line.strip() + '\n')
    for line in datalist:
      line= line.strip()
      if len(line) > 0:
        Out.write(line + '\n')
    Out.close()


  def preProcessData(self, datafile):
    arDatafile = datafile + '.arff'
    self.convertCsv2Arff(datafile, arDatafile)

    (trfile, tsfile) = self.splitTrainTest(arDatafile)


    return trfile, tsfile


  def NB(self, trfile, tsfile, outfile):
    outfile  = outfile + '.NB'
    cmd = 'java -Xmx3g -cp ' + self.wekaDir +' weka.classifiers.bayes.NaiveBayes   -t '\
        + trfile + ' -T ' + tsfile + ' -p 0  > ' + outfile
    print cmd
    os.system(cmd)

    self.basicOutNB = outfile


  def DT(self, trfile, tsfile, outfile):
    outfile  = outfile + '.DT'
    clfPath = 'weka.classifiers.trees.J48'
    option = ' -C 0.25 -M 2 -A  '
    cmd = 'java -Xmx3g -cp ' + self.wekaDir + ' ' + clfPath + option + ' -t ' \
        + trfile + ' -T ' + tsfile + ' -p 0  > ' + outfile
    print cmd
    os.system(cmd)

    self.basicOutDT = outfile


  def MLP(self, trfile, tsfile, outfile):
    outfile  = outfile + '.MLP'
    clfPath = 'weka.classifiers.functions.MultilayerPerceptron'
    cmd = 'java -Xmx3g -cp ' + self.wekaDir + ' ' + clfPath + ' -t ' \
        + trfile + ' -T ' + tsfile + ' -p 0  > ' + outfile
    print cmd
    os.system(cmd)
    #os.system('touch ' + outfile)

    self.basicOutMLP = outfile


  def SMO(self, trfile, tsfile, outfile):
    outfile  = outfile + '.SMO'
    clfPath = 'weka.classifiers.functions.SMO'
    option = ' -C 1.0 -L 0.001 -P 1.0E-12 -N 0 -M -V -1 -W 1 -K \"weka.classifiers.functions.supportVector.PolyKernel -C 250007 -E 1.0\" '
    cmd = 'java -Xmx3g -cp ' + self.wekaDir + ' ' + clfPath + option + ' -t ' \
        + trfile + ' -T ' + tsfile + ' -p 0  > ' + outfile
    print cmd
    os.system(cmd)
    #os.system('touch ' + outfile)

    self.basicOutSMO = outfile


  def Logit(self, trfile, tsfile, outfile):
    outfile  = outfile + '.Logit'
    clfPath = 'weka.classifiers.functions.Logistic'
    option = '  -R 1.0E-8 -M -1  '
    cmd = 'java -Xmx3g -cp ' + self.wekaDir + ' ' + clfPath + option + ' -t ' \
        + trfile + ' -T ' + tsfile + ' -p 0  > ' + outfile
    print cmd
    os.system(cmd)

    self.basicOutLogit = outfile





  def basicClassify(self, trfile, tsfile, outSuffix):

    outfile = tsfile + outSuffix

    self.NB(trfile, tsfile, outfile)
    self.DT(trfile, tsfile, outfile)
    self.MLP(trfile, tsfile, outfile)
    self.SMO(trfile, tsfile, outfile)
    self.Logit(trfile, tsfile, outfile)







######################### metCost #########################33

  def metaNB(self, trfile, tsfile, outfile, costMatrix):
    outfile  = outfile + '.metaNB'
    cmd = 'java -Xmx3g -cp ' + self.wekaDir + ' weka.classifiers.meta.MetaCost -cost-matrix \"'+ costMatrix  + '\" -W weka.classifiers.bayes.NaiveBayes -t '  \
        + trfile + ' -T ' + tsfile + ' -p 0  > ' + outfile
    print 'MetaNB cmd : '
    print cmd
    os.system(cmd)

    self.metaOutNB = outfile

  def metaDT(self, trfile, tsfile, outfile, costMatrix):
    outfile  = outfile + '.metaDT'
    clfPath = 'weka.classifiers.trees.J48'
    option = ' -C 0.25 -M 2 -A  '
    cmd = 'java -Xmx3g -cp ' + self.wekaDir + ' weka.classifiers.meta.MetaCost -cost-matrix \"'+ costMatrix  + '\" -W  ' + clfPath +  ' -t '  \
        + trfile + ' -T ' + tsfile + ' -p 0 ' + ' -- '+ option  +  ' > ' + outfile
    print cmd
    os.system(cmd)
    self.metaOutDT = outfile


  def metaMLP(self, trfile, tsfile, outfile, costMatrix):
    outfile  = outfile + '.metaMLP'
    clfPath = 'weka.classifiers.functions.MultilayerPerceptron'
    cmd = 'java -Xmx3g -cp ' + self.wekaDir + ' weka.classifiers.meta.MetaCost -cost-matrix \"'+ costMatrix  + '\" -W  ' + clfPath + ' -t '  \
        + trfile + ' -T ' + tsfile + ' -p 0  > ' + outfile
    print cmd
    os.system(cmd)
    #os.system('touch ' + outfile)
    self.metaOutMLP = outfile


  def metaSMO(self, trfile, tsfile, outfile, costMatrix):
    outfile  = outfile + '.metaSMO'
    clfPath = 'weka.classifiers.functions.SMO'
    option = ' -C 1.0 -L 0.001 -P 1.0E-12 -N 0 -M -V -1 -W 1 -K \"weka.classifiers.functions.supportVector.PolyKernel -C 250007 -E 1.0\" '
    cmd = 'java -Xmx3g -cp ' + self.wekaDir + ' weka.classifiers.meta.MetaCost -cost-matrix \"'+ costMatrix  + '\" -W  ' + clfPath +  ' -t '  \
        + trfile + ' -T ' + tsfile + ' -p 0 ' + ' -- '+ option +   '  > ' + outfile
    print cmd
    os.system(cmd)
    #os.system('touch ' + outfile)

    self.metaOutSMO = outfile


  def metaLogit(self, trfile, tsfile, outfile, costMatrix):
    outfile  = outfile + '.metaLogit'
    clfPath = 'weka.classifiers.functions.Logistic'
    option = '  -R 1.0E-8 -M -1  '
    cmd = 'java -Xmx3g -cp ' + self.wekaDir + ' weka.classifiers.meta.MetaCost -cost-matrix \"'+ costMatrix  + '\" -W  ' + clfPath + ' -t '  \
        + trfile + ' -T ' + tsfile + ' -p 0 '+' -- ' + option + ' > ' + outfile
    print cmd
    os.system(cmd)
    self.metaOutLogit = outfile



  def metaClassify(self, trfile, tsfile, costMatrix):
    outfile = tsfile + '.clf'

    self.metaNB(trfile, tsfile, outfile, costMatrix)
    self.metaDT(trfile, tsfile, outfile, costMatrix)
    self.metaMLP(trfile, tsfile, outfile, costMatrix)
    self.metaSMO(trfile, tsfile, outfile, costMatrix)
    self.metaLogit(trfile, tsfile, outfile, costMatrix)


#######################################################333################################################
################################ cost sensitive  ################################################


  def csNB(self, trfile, tsfile, outfile, costMatrix):
    outfile  = outfile + '.csNB'
    clfPath = 'weka.classifiers.bayes.NaiveBayes'
    cmd = 'java -Xmx3g -cp ' + self.wekaDir + ' weka.classifiers.meta.CostSensitiveClassifier -cost-matrix \"'+ costMatrix  + '\" -W  ' + clfPath + ' -t '  \
        + trfile + ' -T ' + tsfile + ' -p 0  > ' + outfile
    print 'cost sensitive NB cmd : '
    print cmd
    os.system(cmd)
    self.csOutNB = outfile


  def csDT(self, trfile, tsfile, outfile, costMatrix):
    outfile  = outfile + '.csDT'
    clfPath = 'weka.classifiers.trees.J48'
    option = ' -C 0.25 -M 2 -A  '
    cmd = 'java -Xmx3g -cp ' + self.wekaDir + ' weka.classifiers.meta.CostSensitiveClassifier -cost-matrix \"'+ costMatrix  + '\" -W  ' + clfPath +  ' -t '  \
        + trfile + ' -T ' + tsfile + ' -p 0 '+' -- ' + option + ' > ' + outfile
    print cmd
    os.system(cmd)
    self.csOutDT = outfile

  def csMLP(self, trfile, tsfile, outfile, costMatrix):
    outfile  = outfile + '.csMLP'
    clfPath = 'weka.classifiers.functions.MultilayerPerceptron'
    cmd = 'java -Xmx3g -cp ' + self.wekaDir + ' weka.classifiers.meta.CostSensitiveClassifier -cost-matrix \"'+ costMatrix  + '\" -W  ' + clfPath + ' -t '  \
        + trfile + ' -T ' + tsfile + ' -p 0  > ' + outfile
    print cmd
    os.system(cmd)
    #os.system('touch ' + outfile)
    self.csOutMLP = outfile

  def csSMO(self, trfile, tsfile, outfile, costMatrix):
    outfile  = outfile + '.csSMO'
    clfPath = 'weka.classifiers.functions.SMO'
    option = ' -C 1.0 -L 0.001 -P 1.0E-12 -N 0 -M -V -1 -W 1 -K \"weka.classifiers.functions.supportVector.PolyKernel -C 250007 -E 1.0\" '
    cmd = 'java -Xmx3g -cp ' + self.wekaDir + ' weka.classifiers.meta.CostSensitiveClassifier -cost-matrix \"'+ costMatrix  + '\" -W  ' + clfPath + ' -t '  \
        + trfile + ' -T ' + tsfile + ' -p 0 ' +' -- '+ option + ' > ' + outfile
    print cmd
    os.system(cmd)
    #os.system('touch ' + outfile)
    self.csOutSMO = outfile

  def csLogit(self, trfile, tsfile, outfile, costMatrix):
    outfile  = outfile + '.csLogit'
    clfPath = 'weka.classifiers.functions.Logistic'
    option = '  -R 1.0E-8 -M -1  '
    cmd = 'java -Xmx3g -cp ' + self.wekaDir + ' weka.classifiers.meta.CostSensitiveClassifier -cost-matrix \"'+ costMatrix  + '\" -W  ' + clfPath + ' -t '  \
        + trfile + ' -T ' + tsfile + ' -p 0 ' + ' -- ' + option +'  > ' + outfile
    print cmd
    os.system(cmd)
    self.csOutLogit = outfile



  def costSensitive(self, trfile, tsfile, costMatrix):

    outfile = tsfile + '.clf'

    self.csNB(trfile, tsfile, outfile, costMatrix)

    self.csDT(trfile, tsfile, outfile, costMatrix)
    self.csMLP(trfile, tsfile, outfile, costMatrix)
    self.csSMO(trfile, tsfile, outfile, costMatrix)
    self.csLogit(trfile, tsfile, outfile, costMatrix)



###############################                  ################################################
################################ smote   ################################################

  def smote(self, infile, outfile):
    cmd = 'java -Xmx3g -cp '+self.wekaDir+ ' weka.filters.supervised.instance.SMOTE  -i ' + infile + ' -o ' + outfile + ' -c last  '
    print '\n#### smote command  '
    print cmd
    os.system(cmd)


  def smoteClassify(self, trfile, tsfile):
    stTrfile = trfile + '.smt.arff'
    stTsfile = tsfile + '.smt.arff'
    self.smote(trfile, stTrfile)
    self.smote(tsfile, stTsfile)

    self.basicClassify(stTrfile, stTsfile, '.smote' )


###############################                       ################################################
################################ one class selection  ################################################


  def selectOneSideTrain(self, trfile):
    outfile = trfile + '.oneside'
    cmd = 'java -Xmx3g -cp '+self.wekaDir+ ' weka.filters.unsupervised.attribute.InterquartileRange -i ' + trfile + ' -o ' + outfile + ' -c last  '
    print '======= one side selection : '
    print cmd
    os.system(cmd)

    newTrfile = outfile + '.arff'
    (tlist, datalist) = self.readAFile(outfile)

    Out = open(newTrfile, 'w')
    for line in tlist:
      line = line.strip()
      if line.startswith('@attribute Outlier'):
        continue
      if line.startswith('@attribute ExtremeValue'):
        continue
      Out.write(line + '\n')

    for line in datalist:
      line = line.strip()
      if len(line) > 0:
        termlist = line.split(',')
        if termlist[-3].strip() == 'Y' and  \
            ( termlist[-2].strip().lower()=='yes' or \
            termlist[-1].strip().lower()=='yes' ):
          pass
        else:
          newline = ','.join(termlist[:-2])
          Out.write(newline + '\n')
    Out.close()
    return newTrfile



  def oneSideClassify(self, trfile, tsfile):

    newTrfile = self.selectOneSideTrain(trfile)

    self.basicClassify(newTrfile, tsfile, '.oneside' )



  def runMetaClf( self,  costMatrix):
    self.metaClassify(self.arffTrainFile,  self.arffTestFile, costMatrix)
    self.costSensitive(self.arffTrainFile, self.arffTestFile, costMatrix)


  def runSmoteClf(self):

    self.smoteClassify(self.arffTrainFile, self.arffTestFile)


  def runOneSideClf(self):

    self.oneSideClassify(self.arffTrainFile, self.arffTestFile)




  def main(self):
    datafile = 'data/train-1.csv'
    #datafile = 'data/diabetic_data5.csv'
    (trfile, tsfile) = self.preProcessData(datafile)

    self.arffTrainFile = trfile
    self.arffTestFile  = tsfile

    self.basicClassify(trfile, tsfile, '.basic')

    costMatrix = '[0.0 13632.0; 12186.0 9470.0]'

    #self.metaClassify(trfile, tsfile, costMatrix)

    #self.costSensitive(trfile, tsfile, costMatrix)






if __name__ == '__main__':

  myWeka = CWeka()
  myWeka.main()





