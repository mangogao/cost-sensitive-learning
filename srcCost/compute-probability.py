#!/usr/bin/python
#---coding:utf8----
import sys
import os
import csv
import random
import timeit

def ReadPredFile(infile):
  if not os.path.exists(infile):
    print 'Error! file not exist! file path :', infile
    sys.exit()
  outlist = []
  In = open(infile)
  for line in In.readlines():
    line = line.strip()
    if len(line) > 0:
      terms = line.split()
      #print terms
      if len(terms) != 5 and len(terms) !=4:
        print 'format error : ', line
        continue
      if not terms[0].strip().isdigit():
        print 'not digit : ', terms
        continue

      if len(terms) == 5:
        probScore = float(terms[4])
        if terms[2].startswith('2:'):
          probScore = 1 - probScore
        items = (terms[0], terms[1], terms[2], probScore, terms[3])
      if len(terms) == 4:
        probScore = float(terms[3])
        if terms[2].startswith('2:'):
          probScore = 1 - probScore
        items = (terms[0], terms[1], terms[2], probScore, '')
     # if len(terms) == 5:
      #  items = (terms[0], terms[1], terms[2], float(terms[4]), terms[3])
     # if len(terms) == 4:
      #  items = (terms[0], terms[1], terms[2], float(terms[3]), '')
      #print items
      outlist.append( items )
     # print ' ID : ', terms[0], ' actual : ', terms[1], ' predict :', terms[2], ' prob: ', terms[3], '  dist :', terms[4]
  In.close()
  return outlist








#if __name__ == '__main__':

def mainOurMethod3(infile):
    start = timeit.default_timer()
    #infile ='/Users/YuanyuanGao/Desktop/CTG/DT.csv'
    infile = infile.strip()
    resultlist = ReadPredFile(infile)
    l = len(resultlist)
    classifylist = []
    investlist = []
    prolist = []
    totalcost = 0
    investcount = 0
    falsepos =0
    falseneg =0
    Vnum = 0




    for i in range(0,l):
        pro = resultlist[i][3]
        prolist.append(pro)
        #classifylist.append(cl)
        #investlist.append(invest)

    #stop = timeit.default_timer()

    #print "execution time:", stop - start

    return prolist
