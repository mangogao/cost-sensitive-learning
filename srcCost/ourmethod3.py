#!/usr/bin/python
#---coding:utf8----
import sys
import os
import csv
import random
import timeit

def ReadPredFile(infile):
  if not os.path.exists(infile):
    print 'Error! file not exist! file path :', infile
    sys.exit()
  outlist = []
  In = open(infile)
  for line in In.readlines():
    line = line.strip()
    if len(line) > 0:
      terms = line.split()
      #print terms
      if len(terms) != 5 and len(terms) !=4:
        print 'format error : ', line
        continue
      if not terms[0].strip().isdigit():
        print 'not digit : ', terms
        continue

      if len(terms) == 5:
        probScore = float(terms[4])
        if terms[2].startswith('2:'):
          probScore = 1 - probScore
        items = (terms[0], terms[1], terms[2], probScore, terms[3])
      if len(terms) == 4:
        probScore = float(terms[3])
        if terms[2].startswith('2:'):
          probScore = 1 - probScore
        items = (terms[0], terms[1], terms[2], probScore, '')
     # if len(terms) == 5:
      #  items = (terms[0], terms[1], terms[2], float(terms[4]), terms[3])
     # if len(terms) == 4:
      #  items = (terms[0], terms[1], terms[2], float(terms[3]), '')
      #print items
      outlist.append(items)
     # print ' ID : ', terms[0], ' actual : ', terms[1], ' predict :', terms[2], ' prob: ', terms[3], '  dist :', terms[4]
  In.close()
  return outlist




def MinCost(pro):
    #V=442
    '''
    p11 = 1
    p00 = 0.435
    p10 = 0.435
    p01 = 0.435
    '''
    p11 = 1
    p00 = 0.875
    p10 = 0.875
    p01 = 0.875

    #c11 = 0
   # p10 = 0.3
   # p10 = 0.5
   # p10 =0.7
   # p10 = 0.95

    c11new = 0
    c00new= 2740
    c10new = 5456
    c01new = 6902
    '''

    c11new = 0
    c00new= 1370
    c10new = 2728
    c01new = 3451
    '''
    c11 = 0
    c00 = 9470
    c10 = 12186
    c01 = 13632
#    V=2060
    V = 1636
   # V = 1918.75
   # V = 818
   # V = 1863
   # V = 1552.5
   # V = 3000
   # V = 5000
   # V=1000
   # V = 500
    Cost1 = V+pro*p11*c11new+pro*(1-p11)*c11+(1-pro)*p10*c10new+(1-pro)*(1-p10)*c10
    Cost2 = V+pro*p01*c01new+pro*(1-p01)*c01+(1-pro)*p00*c00new+(1-pro)*(1-p00)*c00
    Cost3 = pro*c11+(1-pro)*c10
    Cost4 = pro*c01+(1-pro)*c00



    if (Cost1 == min(Cost1,Cost2,Cost3,Cost4)):
        classify = 1
        invest = 1
    elif (Cost2 == min(Cost1,Cost2,Cost3,Cost4)):
        classify = 0
        invest = 1
    elif (Cost3 == min(Cost1,Cost2,Cost3,Cost4)):
          classify = 1
          invest = 0
    elif (Cost4 == min(Cost1,Cost2,Cost3,Cost4)):
          classify = 0
          invest = 0

    return (classify, invest)



#if __name__ == '__main__':

def mainOurMethod3(infile):
    start = timeit.default_timer()
    #infile ='/Users/YuanyuanGao/Desktop/CTG/DT.csv'
    infile = infile.strip()
    resultlist = ReadPredFile(infile)
    l = len(resultlist)
    classifylist = []
    investlist = []
    totalcost = 0
    investcount = 0
    falsepos =0
    falseneg =0
    Vnum = 0


    '''
    p11 = 1
    p00 = 0.435
    p10 = 0.435
    p01 = 0.435
    '''
    p11 = 1
    p00 = 0.875
    p10 = 0.875
    p01 = 0.875

   # p10 = 0.3
   # p10 = 0.5
   # p10 = 0.7
   # p10 = 0.95
    '''
    c11new = 0
    c00new= 1370
    c10new = 2728
    c01new = 3451
    '''
    c11new = 0
    c00new= 2740
    c10new = 5456
    c01new = 6902

    c11 = 0
    c00 = 9470
    c10 = 12186
    c01 = 13632
    '''
    c11new = 0
    c00new= 4289
    c10new = 12000

   # c10new = 8632
    c01new = 5456
    c11 = 0
    c00 = 10243
    c10 = 14581
    c01 = 11405
    '''
   # V=2060
    V=1636
   # V = 1918.75
   # V = 818
   # V = 1863
   # V = 1552.5
   # V = 3000
   # V = 5000
   # V=1000
   # V = 500
    randomlist = [0]*100

    print l
    for i in range(0,l):
        cl,invest = MinCost(resultlist[i][3])
        print '####Probabilities', (resultlist[i][3])
        classifylist.append(cl)
        investlist.append(invest)
    for i in range(0, l):
        actual = resultlist[i][1]
        for t in range (0,100):
            prob11 = random.uniform(0,1)
            prob00 = random.uniform(0,1)
            prob10 = random.uniform(0,1)
            prob01 = random.uniform(0,1)
            #randomlist.append(prob)

            if (actual== "1:Y" and classifylist[i]==0 and investlist[i] == 0):
                totalcost = totalcost+c01
                investcount = investcount
                falseneg = falseneg+1
                #print "classify as: ", classifylist[i], " real class: ", actual, " wrong prediction; ", " no invest"

            elif(actual=="2:N" and classifylist[i] == 1 and investlist[i] ==0):
                totalcost = totalcost+c10
                investcount = investcount
                falsepos = falsepos+1
                #print "classify as: ", classifylist[i], " real class: ", actual, " wrong prediction; ", " no invest"

            elif(actual== "1:Y" and classifylist[i]==0 and investlist[i] == 1 and prob01 <= p01):
                totalcost = totalcost+c01new + V
                investcount = investcount +1
                falseneg = falseneg+1
                Vnum = Vnum+1
                #print "classify as: ", classifylist[i], " real class: ", actual, " wrong prediction; ", " with invest"

            elif(actual== "1:Y" and classifylist[i]==0 and investlist[i] == 1 and prob01 > p01):
                totalcost = totalcost+c01 + V
                investcount = investcount +1
                falseneg = falseneg+1
                Vnum = Vnum+1
                #print "classify as: ", classifylist[i], " real class: ", actual, " wrong prediction; ", " with invest"

            elif(actual=="2:N" and classifylist[i] == 1 and investlist[i] ==1 and prob10 <= p10):
                totalcost = totalcost+c10new+ V
                investcount = investcount +1
                falsepos = falsepos+1
                Vnum = Vnum+1
                #print "classify as: ", classifylist[i], " real class: ", actual, " wrong prediction; ", " with invest"

            elif(actual=="2:N" and classifylist[i] == 1 and investlist[i] ==1 and prob10 > p10):
                totalcost = totalcost+c10+ V
                investcount = investcount +1
                falsepos = falsepos+1
                Vnum = Vnum+1
                #print "classify as: ", classifylist[i], " real class: ", actual, " wrong prediction; ", " with invest"

            elif(actual== "1:Y" and classifylist[i]==1 and investlist[i] == 0):
                totalcost = totalcost+c11
                investcount = investcount
                #print "classify as: ", classifylist[i], " real class: ", actual, " right prediction; ", " no invest"
            elif(actual== "1:Y" and classifylist[i]==1 and investlist[i] == 1 and prob11 <= p11):
                totalcost = totalcost+c11new+V
                investcount = investcount +1
                Vnum = Vnum+1
                #print "classify as: ", classifylist[i], " real class: ", actual, " right prediction; ", " with invest"

            elif(actual== "1:Y" and classifylist[i]==1 and investlist[i] == 1 and prob11 > p11):
                totalcost = totalcost+c11+V
                investcount = investcount +1
                Vnum = Vnum+1
                #print "classify as: ", classifylist[i], " real class: ", actual, " right prediction; ", " with invest"
            elif(actual=="2:N" and classifylist[i] == 0 and investlist[i] ==0):
                totalcost = totalcost+c00
                investcount = investcount
                #print "classify as: ", classifylist[i], " real class: ", actual, " right prediction; ", " no invest"
            elif(actual=="2:N" and classifylist[i] == 0 and investlist[i] ==1 and prob00 <= p00):
                totalcost = totalcost+c00new+V
                investcount = investcount +1
                Vnum = Vnum+1

            elif(actual=="2:N" and classifylist[i] == 0 and investlist[i] ==1 and prob00 > p00):
                totalcost = totalcost+c00+V
                investcount = investcount +1
                Vnum = Vnum+1
                #print "classify as: ", classifylist[i], " real class: ", actual, " right prediction; ", " with invest"
    avgTotalCost =  totalcost/100
    avgFalsePos  =  falsepos/100
    avgFalseNeg  =  falseneg/100
    avgVnum = Vnum/100
    print investcount/100

    print totalcost/100
    print falsepos/100
    print falseneg/100
    print investcount/100
    #stop = timeit.default_timer()

    #print "execution time:", stop - start

    return avgTotalCost, avgFalsePos, avgFalseNeg, avgVnum
