#!/usr/bin/python
#---coding:utf8----
import sys
import os
import csv
def Bak_ReadPredFile(infile):
  if not os.path.exists(infile):
    print 'Error! file not exist! file path :', infile
    sys.exit()
  outlist = []
  In = open(infile)
  for line in In.readlines():
    line = line.strip()
    if len(line) > 0:
      terms = line.split(',')
      if len(terms) != 5:
           print 'format error : ', line
      items = (terms[0], terms[1], terms[2], float(terms[3]), float(terms[4]))
      outlist.append( items )
      #print ' ID : ', terms[0], ' actual : ', terms[1], ' predict :', terms[2], ' prob: ', terms[3], '  dist :', terms[4]
  In.close()
  return outlist


def ReadPredFile(infile):
  if not os.path.exists(infile):
    print 'Error! file not exist! file path :', infile
    sys.exit()
  outlist = []
  In = open(infile)
  for line in In.readlines():
    line = line.strip()
    if len(line) > 0:
      terms = line.split()
      #print terms
      if len(terms) != 5 and len(terms) !=4:
        print 'format error : ', line
        continue
      if not terms[0].strip().isdigit():
        print 'not digit : ', terms
        continue

      if len(terms) == 5:
        probScore = float(terms[4])
        if terms[2].startswith('2:'):
          probScore = 1 - probScore
        items = (terms[0], terms[1], terms[2], probScore, terms[3])
      if len(terms) == 4:
        probScore = float(terms[3])
        if terms[2].startswith('2:'):
          probScore = 1 - probScore
        items = (terms[0], terms[1], terms[2], probScore, '')
     # if len(terms) == 5:
      #  items = (terms[0], terms[1], terms[2], float(terms[4]), terms[3])
     # if len(terms) == 4:
      #  items = (terms[0], terms[1], terms[2], float(terms[3]), '')
      #print items
      outlist.append( items )
     # print ' ID : ', terms[0], ' actual : ', terms[1], ' predict :', terms[2], ' prob: ', terms[3], '  dist :', terms[4]
  In.close()
  return outlist






def Classification(pro):

        c11 = 0
        c00 = 9470
        c10 = 12186
        c01 = 13632
        '''
        c11 = 0
        c00 = 10243
        c10 = 14581
        c01 = 11405
        '''
        Cost1 = pro*c11 + (1-pro)*c10
        Cost2 = (1-pro)*c00 + pro*c01
        if Cost1>=Cost2:
            classify = 0
        elif Cost1<Cost2:
            classify = 1
        return classify

#if __name__ == '__main__':
def CSoldMatrix(infile):
#    infile = '/Users/YuanyuanGao/Desktop/CTG/SMO.csv'
    infile = infile.strip()
    resultlist = ReadPredFile(infile)
    totalcost=0
    falsepos =0
    falseneg =0
    Vnum =0
    l = len(resultlist)
    '''
    c11 = 0
    c00 = 10243
    c10 = 14581
    c01 = 11405
    '''

    c11 = 0
    c00 = 9470
    c10 = 12186
    c01 = 13632
    classifylist = []

    for i in range(0,l):
        cl = Classification(resultlist[i][3])
        classifylist.append(cl)

    for i in range(0, l):
        #actual = resultlist[i][1]
        #predict = resultlist[i][2]
        if (resultlist[i][1]== "1:Y" and classifylist[i] == 0):
            totalcost = totalcost+c01
            falsepos = falsepos+1
            #print "classify as: ", classifylist[i], " real class: ", resultlist[i][1], " wrong prediction; "

        elif(resultlist[i][1]=="2:N" and classifylist[i] == 1):
            totalcost = totalcost+c10
            falseneg = falseneg+1
            #print "classify as: ",classifylist[i], " real class: ", resultlist[i][1], " wrong prediction; "


        elif(resultlist[i][1]== "1:Y" and classifylist[i] == 1):
            totalcost = totalcost+c11
            #print "classify as: ", classifylist[i], " real class: ", resultlist[i][1], " right prediction; "


        elif(resultlist[i][1]=="2:N" and classifylist[i] == 0):
            totalcost = totalcost+c00

            #print "classify as: ",classifylist[i], " real class: ", resultlist[i][1], " right prediction; "

    print totalcost
    print falseneg
    print falsepos


    return totalcost, falsepos, falseneg, Vnum

