#!/usr/bin/python
#---coding:utf8----
import sys
import os
import csv
import random

def bak_ReadPredFile(infile):
  if not os.path.exists(infile):
    print 'Error! file not exist! file path :', infile
    sys.exit()
  outlist = []
  In = open(infile)
  for line in In.readlines():
    line = line.strip()
    if len(line) > 0:
      terms = line.split(',')
      if len(terms) != 5:
          print 'format error : ', line
      items = (terms[0], terms[1], terms[2], float(terms[3]), float(terms[4]))
      outlist.append( items )
      #print ' ID : ', terms[0], ' actual : ', terms[1], ' predict :', terms[2], ' prob: ', terms[3], '  dist :', terms[4]
  In.close()
  return outlist


def ReadPredFile(infile):
  if not os.path.exists(infile):
    print 'Error! file not exist! file path :', infile
    sys.exit()
  outlist = []
  In = open(infile)
  for line in In.readlines():
    line = line.strip()
    if len(line) > 0:
      terms = line.split()
      #print terms
      if len(terms) != 5 and len(terms) !=4:
        print 'format error : ', line
        continue
      if not terms[0].strip().isdigit():
        print 'not digit : ', terms
        continue

      if len(terms) == 5:
        items = (terms[0], terms[1], terms[2], float(terms[4]), terms[3])
      if len(terms) == 4:
        items = (terms[0], terms[1], terms[2], float(terms[3]), '')
      #print items
      outlist.append( items )
     # print ' ID : ', terms[0], ' actual : ', terms[1], ' predict :', terms[2], ' prob: ', terms[3], '  dist :', terms[4]
  In.close()
  return outlist

#if __name__ == '__main__':
def OriginalNonCSMethod(infile):
    #infile = '/Users/YuanyuanGao/Desktop/untitled folder/NNMETA15.csv'
    infile = infile.strip()
    resultlist = ReadPredFile(infile)
    totalcost=0
    falsepos =0
    falseneg =0
    Vnum = 0
    l = len(resultlist)

    c11 = 0
    c00 = 9470
    c10 = 12186
    c01 = 13632
   # c11 = 0
   # c00 = 10243
   # c10 = 14581
   # c01 = 11405

    for i in range(0, l):
        #actual = resultlist[i][1]
        #predict = resultlist[i][2]
        if (resultlist[i][1]== "1:Y" and resultlist[i][2] == "2:N"):
            totalcost = totalcost+c01
            falseneg = falseneg+1
            #print "classify as: ", resultlist[i][2], " real class: ", resultlist[i][1], " wrong prediction; "

        elif(resultlist[i][1]=="2:N" and resultlist[i][2] == "1:Y"):
            totalcost = totalcost+c10
            falsepos = falsepos+1
            #print "classify as: ", resultlist[i][2], " real class: ", resultlist[i][1], " wrong prediction; "


        elif(resultlist[i][1]== "1:Y" and resultlist[i][2]== "1:Y"):
            totalcost = totalcost+c11
            #print "classify as: ", resultlist[i][2], " real class: ", resultlist[i][1], " right prediction; "


        elif(resultlist[i][1]=="2:N" and resultlist[i][2] == "2:N"):
            totalcost = totalcost+c00

            #print "classify as: ", resultlist[i][2], " real class: ", resultlist[i][1], " right prediction; "

    print totalcost
    print falsepos
    print falseneg

    return totalcost, falsepos, falseneg, Vnum

