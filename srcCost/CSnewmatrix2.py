#!/usr/bin/python
#---coding:utf8----
import sys
import os
import csv
import timeit
import random

def bak_ReadPredFile(infile):
  if not os.path.exists(infile):
    print 'Error! file not exist! file path :', infile
    sys.exit()
  outlist = []
  In = open(infile)
  for line in In.readlines():
    line = line.strip()
    if len(line) > 0:
      terms = line.split(',')
      if len(terms) != 5:
        print 'format error : ', line
      items = (terms[0], terms[1], terms[2], float(terms[3]), float(terms[4]))
      outlist.append( items )
      #print ' ID : ', terms[0], ' actual : ', terms[1], ' predict :', terms[2], ' prob: ', terms[3], '  dist :', terms[4]
  In.close()
  return outlist


def ReadPredFile(infile):
  if not os.path.exists(infile):
    print 'Error! file not exist! file path :', infile
    sys.exit()
  outlist = []
  In = open(infile)
  for line in In.readlines():
    line = line.strip()
    if len(line) > 0:
      terms = line.split()
      #print terms
      if len(terms) != 5 and len(terms) !=4:
        print 'format error : ', line
        continue
      if not terms[0].strip().isdigit():
        print 'not digit : ', terms
        continue

      if len(terms) == 5:
        probScore = float(terms[4])
        if terms[2].startswith('2:'):
          probScore = 1 - probScore
        items = (terms[0], terms[1], terms[2], probScore, terms[3])
      if len(terms) == 4:
        probScore = float(terms[3])
        if terms[2].startswith('2:'):
          probScore = 1 - probScore
        items = (terms[0], terms[1], terms[2], probScore, '')
     # if len(terms) == 5:
      #  items = (terms[0], terms[1], terms[2], float(terms[4]), terms[3])
     # if len(terms) == 4:
      #  items = (terms[0], terms[1], terms[2], float(terms[3]), '')
      #print items
      outlist.append( items )
     # print ' ID : ', terms[0], ' actual : ', terms[1], ' predict :', terms[2], ' prob: ', terms[3], '  dist :', terms[4]
  In.close()
  return outlist





def Classification(pro):
        '''
        c11new = 0
        c00new= 4289
        c10new = 8632
        c01new = 5456
        '''

        c11new = 0
        c00new= 2740
        c10new = 5456
        c01new = 6902
        pstar = float(c10new-c00new)/(float(c10new+c01new-c00new-c11new))

        if(pro>=pstar):
            classify = 1

        elif(pro<pstar):
            classify = 0

        return classify

#if __name__ == '__main__':

def CSnewMatrix(infile):
    start = timeit.default_timer()
    #infile ='/Users/YuanyuanGao/Desktop/CTG/NN.csv'
    infile = infile.strip()
    resultlist = ReadPredFile(infile)
    totalcost=0
    falsepos =0
    falseneg =0
    Vnum =0
    l = len(resultlist)

    c11new = 0
    c00new= 2740
    c10new = 5456
    c01new = 6902
    c11 = 0
    c00 = 9470
    c10 = 12186
    c01 = 13632

    '''
    c11new = 0
    c00new= 4289
    c10new = 8632
    c01new = 5456
    c11 = 0
    c00 = 10243
    c10 = 14581
    c01 = 11405
    '''
   # V=2060
   # V = 1242
    V = 1636
   # V =818
    classifylist = []
    randomlist = [0]*100

    p11 = 1
    p00 = 0.875
    p10 = 0.875
    p01 = 0.875
    '''

    p11 = 1
    p00 = 0.435
    p10 = 0.435
    p01 = 0.435
    '''
    for i in range(0,l):
        cl = Classification(resultlist[i][3])
        classifylist.append(cl)

    for i in range(0, l):
        #actual = resultlist[i][1]
        #predict = resultlist[i][2]
        for t in range (0,100):
            prob11 = random.uniform(0,1)
            prob00 = random.uniform(0,1)
            prob10 = random.uniform(0,1)
            prob01 = random.uniform(0,1)
            #print prob11
            if (resultlist[i][1]== "1:Y" and classifylist[i] == 0 and prob01<=p01):
                totalcost = totalcost+c01new+V
                falseneg = falseneg+1
                Vnum = Vnum+1
            # print "classify as: ", classifylist[i], " real class: ", resultlist[i][1], " wrong prediction; "
            elif (resultlist[i][1]== "1:Y" and classifylist[i] == 0 and prob01>p01):
                totalcost = totalcost+c01+V
                falseneg = falseneg+1
                Vnum = Vnum+1

            elif(resultlist[i][1]=="2:N" and classifylist[i] == 1 and prob10 <= p10):
                totalcost = totalcost+c10new+V
                falsepos = falsepos+1
                Vnum = Vnum+1
                #print "classify as: ",classifylist[i], " real class: ", resultlist[i][1], " wrong prediction; "
            elif(resultlist[i][1]=="2:N" and classifylist[i] == 1 and prob10 > p10):
                totalcost = totalcost+c10+V
                falsepos = falsepos+1
                Vnum = Vnum+1
            elif(resultlist[i][1]== "1:Y" and classifylist[i] == 1 and prob11 <= p11):
                totalcost = totalcost+c11new+V
                Vnum = Vnum+1
                #print "classify as: ", classifylist[i], " real class: ", resultlist[i][1], " right prediction; "
            elif(resultlist[i][1]== "1:Y" and classifylist[i] == 1 and prob11 > p11):
                totalcost = totalcost+c11+V
                Vnum = Vnum+1
                #print "classify as: ", classifylist[i], " real class: ", resultlist[i][1], " right prediction; "

            elif(resultlist[i][1]=="2:N" and classifylist[i] == 0 and prob00 <=p00):
                totalcost = totalcost+c00new+V
                Vnum = Vnum+1
                #print "classify as: ", classifylist[i], " real class: ", resultlist[i][1], " right prediction; "
            elif(resultlist[i][1]=="2:N" and classifylist[i] == 0 and prob00 > p00):
                totalcost = totalcost+c00+V
                Vnum = Vnum+1

    print totalcost/100
    print falsepos/100
    print falseneg/100

    avgTotalCost =  totalcost/100
    avgFalsePos  =  falsepos/100
    avgFalseNeg  =  falseneg/100
    avgVnum = Vnum/100

    stop = timeit.default_timer()

    #print "execution time:", stop - start
    return avgTotalCost, avgFalsePos, avgFalseNeg, avgVnum

